<?php

namespace App\Entity;

use Firebase\JWT\JWT;
use App\Repository\TokenRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Firebase\JWT\Key;

#[ORM\Entity(repositoryClass: TokenRepository::class)]
class Token
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $accessToken = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $accessTokenExpiresAt = null;

    #[ORM\Column(length: 255)]
    private ?string $refreshToken = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $refreshTokenExpiresAt = null;

    #[ORM\OneToOne(inversedBy: 'token', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?Utilisateur $utilisateur = null;

    private static string $lifetimeAccessToken = "PT60M";//"PT9999M";
    private static string $lifetimeRefreshToken = "PT120M";
    private static string $algo = "HS256";
    private static string $keypass = "c319bcf9e2aae732f37581d77c68a5221ace16c0f3cffaa19ef892a3609dae03";


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): static
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getAccessTokenExpiresAt(): ?string
    {
        return $this->accessTokenExpiresAt->getTimestamp();
    }

    public function setAccessTokenExpiresAt(?\DateTimeInterface $accessTokenExpiresAt = null): static
    {
        if ($accessTokenExpiresAt == null) $accessTokenExpiresAt = (new \DateTime())->add(new \DateInterval(self::$lifetimeAccessToken));
        $this->accessTokenExpiresAt = $accessTokenExpiresAt;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(string $refreshToken): static
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getRefreshTokenExpiresAt(): ?string
    {
        return $this->refreshTokenExpiresAt->getTimestamp();
    }

    public function setRefreshTokenExpiresAt(?\DateTimeInterface $refreshTokenExpiresAt = null): static
    {
        if ($refreshTokenExpiresAt == null) $refreshTokenExpiresAt = (new \DateTime())->add(new \DateInterval(self::$lifetimeRefreshToken));
        $this->refreshTokenExpiresAt = $refreshTokenExpiresAt;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(Utilisateur $utilisateur): static
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function JWT(string $uuid): static
    {
        $this->setAccessTokenExpiresAt();
        $accesstoken = $this->encode($uuid, $this->getAccessTokenExpiresAt());
        $this->setAccessToken($accesstoken);

        $this->setRefreshTokenExpiresAt();
        $refreshtoken = $this->encode($uuid, $this->getRefreshTokenExpiresAt());
        $this->setRefreshToken($refreshtoken);

        return $this;
    }

    public function refreshToken(string $uuid): static
    {
        $this->setAccessTokenExpiresAt();
        $accesstoken = $this->encode($uuid, $this->getAccessTokenExpiresAt());
        $this->setAccessToken($accesstoken);

        return $this;
    }

    private function encode(string $uuid, string $expiresAT) : string {
        return JWT::encode(["sub" => $uuid, "exp" => $expiresAT], self::$keypass, self::$algo);
    }

    public function decodeJWT(string $jwt) {
        return (array) JWT::decode($jwt, new Key(self::$keypass, self::$algo));
    }
}
