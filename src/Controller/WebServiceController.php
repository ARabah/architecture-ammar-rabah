<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Film;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

#[Route('/film', name: 'app_film', stateless: true)]
class WebServiceController extends AbstractController
{
    private $serializer;
    private $entityManager;
    private $filesystem;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $entityManager, Filesystem $filesystem)
    {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->filesystem = $filesystem;
    }

    #[Route('/web/service', name: '_web_service', stateless: true)]
    public function index(Request $request): JsonResponse
    {
        // dd($request->getContent());
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/WebServiceController.php',
        ]);
    }

    #[Route('/getFilms/{page}', name: '_pagination', methods: ['GET'], stateless: true)]
    public function getFilms(Request $request, $page, EntityManagerInterface $entityManager): Response
    {
        $content_type = $request->headers->get('content-type');


        if ($page == 'all' || $page == 'tous') {
            
            $films = array();
            if ($content_type == "application/hal+json" || $content_type == "application/hal+xml") {
                $films["links"] = array(
                    "href" => "/getFilms/" . $page,
                    "rel" => "film",
                    "type" => "GET",
                );
            }
            foreach (json_decode($this->serializer->serialize($entityManager->getRepository(Film::class)->findAll(), "json")) as $film) {
                $films[] = array(
                    "id" => intval($film->id),
                    "nom" => $film->nom,
                    "description" => $film->description,
                    "date" => $film->date,
                    "note" => intval($film->note),
                    "categorie" => json_decode($this->serializer->serialize(($this->entityManager->getRepository(Film::class)->find($film->id)->getCategories()->toArray()), "json"))
                );
            }
            return $this->handleResponse($content_type, (new Film), $films);
        }

        try {
            if (!preg_match('/\d+/', $page)) {
                throw new \Exception("numéro de la page manquante", 400);
            }

            $list = array_chunk(json_decode($this->serializer->serialize($entityManager->getRepository(Film::class)->findAll(), "json")), 10);
            if (count($list) >= $page) {
                if ($content_type == "application/hal+json" || $content_type == "application/hal+xml") {
                    $arr["links"] = array(
                        "href" => "/getFilms/" . $page,
                        "rel" => "film",
                        "type" => "GET",
                    );
                }
                $arr["links"] = array("pages" => $page . "/" . count($list));
                
                foreach ($list[$page == 0 ? 0 : $page - 1] as $film) {
                    $arr[] = array(
                        "id" => intval($film->id),
                        "nom" => $film->nom,
                        "description" => $film->description,
                        "date" => $film->date,
                        "note" => intval($film->note),
                        "categorie" => json_decode($this->serializer->serialize(($this->entityManager->getRepository(Film::class)->find($film->id)->getCategories()->toArray()), "json"))
                    );
                }
                
                return $this->handleResponse($content_type, (new Film), $arr);
            } else
                throw new \Exception("Il y a " . count($list) . " pages", 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }
    }


    #[Route('/getFilms/', name: '_info_pagination', methods: ['GET'], stateless: true)]
    public function infogetFilms(EntityManagerInterface $entityManager): Response
    {
        return new Response("Pour avoir toutes les pages, il faut faire /getFilms/{all|tous} et pour avoir page par page, il faut faire /getFilms/{numéro de la page}", 418);
    }

    #[Route('/{id}', name: '_get_film', methods: ['GET'], requirements: ['id' => '\d+'], stateless: true)]
    public function getFilm(Request $request, int $id): JsonResponse|Response
    {
        $content_type = $request->headers->get('content-type');

        try {
            // Récupère le film depuis la base de données par son ID
            $film = $this->entityManager->getRepository(Film::class)->find($id);

            if ($film === null)
                return $this->json(['erreur' => 'Resource Introuvable'], 422);

            if ($content_type == "application/hal+json" || $content_type == "application/hal+xml") {
                $film2 = json_decode($this->serializer->serialize($film, "json"), true);
                
                $films = array();
                $films["links"] = array(
                    "href" => "/" . $id,
                    "rel" => "film",
                    "type" => "GET"
                );
                $films["id"] = $film2["id"];
                $films["nom"] = $film2["nom"];
                $films["description"] = $film2["description"];
                $films["date"] = (new \DateTime($film2["date"]))->format("Y-m-d");
                $fimls["note"] = intval($film2["note"]);
                $films["categorie"] = ($this->entityManager->getRepository(Film::class)->find($films["id"])->getCategories()->toArray());
                return $this->handleResponse($content_type, $film, $films);
            }
            
            $film = json_decode($this->serializer->serialize($film, "json"), true);
            $film["categorie"] = ($this->entityManager->getRepository(Film::class)->find($film["id"])->getCategories()->toArray());


            // Gère la réponse en fonction du content-type
            return $this->handleResponse($content_type, (new Film), $film);

        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 500);
        }
    }

    #[Route('/{id}', name: '_delete_film', methods: ['DELETE'], requirements: ['id' => '\d+'], stateless: true)]
    public function deleteFilm(int $id): JsonResponse
    {
        try {
            // Récupère le film depuis la base de données par son ID
            $film = $this->entityManager->getRepository(Film::class)->find($id);

            if ($film === null)
                return $this->json(['erreur' => 'La ressource n’existe plus'], 410);

            // Supprime le film de la base de données
            $this->entityManager->remove($film);
            $this->entityManager->flush();

            return $this->json(['message' => 'Resource Modifiée'], 200);

        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 500);
        }
    }

    #[Route('/creation/{nom}/{description}/{date}/{note}', name: '_create_film', methods: ['POST'], stateless: true)]
    public function createFilm(string $nom = null, string $description = null, mixed $date = new \DateTime(), int $note = 0): JsonResponse
    {
        try {
            // Crée un nouvel objet Film avec les données reçues
            $newFilm = new Film();
            $newFilm->setNom($nom);
            $newFilm->setDescription($description);
            $newFilm->setDate(new \DateTime($date));
            $newFilm->setNote($note);

            // Persiste et flush le nouvel objet Film dans la base de données
            $this->entityManager->persist($newFilm);
            $this->entityManager->flush();

            return $this->json($newFilm, 201);

        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 500);
        }
    }

    #[Route('/{id}/modif/{nom}/{description}/{date}/{note}', name: '_update_film', methods: ['PUT', 'PATCH'], stateless: true)]
    public function updateFilm(Request $request, int $id, string $nom, string $description, string $date, int $note): JsonResponse
    {
        try {
            
            // Récupère le film depuis la base de données par son ID
            $film = $this->entityManager->getRepository(Film::class)->find($id);

            if ($film === null)
                return $this->json(['erreur' => 'Resource Introuvable'], 422);

            switch ($request->server->get("REQUEST_METHOD")) {
                case 'PUT':
                     // Met à jour les propriétés du film avec les nouvelles valeurs
                    $film->setNom($nom);
                    $film->setDescription($description);
                    $film->setDate(new \DateTime($date));
                    $film->setNote($note);

                    // Flush les modifications dans la base de données
                    $this->entityManager->flush();
                    break;
                
                case 'PATCH':
                    // Met à jour les propriétés du film avec les nouvelles valeurs

                    if (($nom != "" || $nom != null) && strlen($nom) < 128)
                        $film->setNom($nom);
                    if (($description != "" || $description != null) && strlen($description) < 2048)
                        $film->setDescription($description);
                    if (($date != "" || $date != null) && (date("Y-m-d", $date) || date("d-m-Y", $date)))
                        $film->setDate(new \DateTime($date));
                    if ($note != null && intval($note) > 0 && intval($note) < 0)
                        $film->setNote($note);
        
                    // Flush les modifications dans la base de données
                    $this->entityManager->flush();
                    break;
            }

            return $this->json(['message' => 'Resource Modifiée'], 200);

        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 422);
        }
    }

    #[Route('/recherche/{typeRecherche}/{value}', name: '_recherche', methods: ['GET'], requirements: ['typeRecherche' => 'titre|description'], stateless: true)]
    public function recherche(Request $request, string $typeRecherche, string $value): JsonResponse|Response
    {
        $content_type = $request->headers->get('content-type');

        // $res = $this->entityManager->getRepository(Film::class)->selectBy($typeRecherche, $value);
        $res = $this->entityManager->getRepository(Film::class)->findOneBy(
            [$typeRecherche == "titre" ? 'nom' : $typeRecherche => $value]
        );

        if ($res === null)
            return $this->json(['erreur' => 'Resource Introuvable'], 422);


        if ($content_type == "application/hal+json") {
            $film2 = json_decode($this->serializer->serialize($res, "json"), true);
            
            $films = array();
            $films["links"] = array(
                "href" => "/" . $typeRecherche . "/" . $value,
                "rel" => "film",
                "type" => "GET"
            );
            $films["id"] = $film2["id"];
            $films["nom"] = $film2["nom"];
            $films["description"] = $film2["description"];
            $films["date"] = (new \DateTime($film2["date"]))->format("Y-m-d");
            $films["note"] = $film2["note"];
            $films["categorie"] = ($this->entityManager->getRepository(Film::class)->find($films["id"])->getCategories()->toArray());
            return $this->handleResponse($content_type, (new Film()), $films);
        } else if ($content_type == "application/hal+xml") {
            $film2 = json_decode($this->serializer->serialize($res, "json"), true);
            
            $films = array();
            $films["_links"] = array(
                "href" => "/" . $typeRecherche . "/" . $value,
                "rel" => "film",
                "type" => "GET"
            );
            $films["id"] = $film2["id"];
            $films["nom"] = $film2["nom"];
            $films["description"] = $film2["description"];
            $films["date"] = (new \DateTime($film2["date"]))->format("Y-m-d");
            $films["note"] = $film2["note"];
            $films["categorie"] = ($this->entityManager->getRepository(Film::class)->find($films["id"])->getCategories()->toArray());
            return $this->handleResponse($content_type, (new Film()), $films);
        }

        $film2 = json_decode($this->serializer->serialize($res, "json"), true);
        
        $films = array();
        $films["id"] = $film2["id"];
        $films["nom"] = $film2["nom"];
        $films["description"] = $film2["description"];
        $films["date"] = (new \DateTime($film2["date"]))->format("Y-m-d");
        $films["note"] = $film2["note"];
        $films["categorie"] = ($this->entityManager->getRepository(Film::class)->find($films["id"])->getCategories()->toArray());

        return $this->handleResponse($content_type, (new Film), $films);
    }

    #[Route('/{id}/categories', name: '_get_film_categories', methods: ['GET'], requirements: ['id' => '\d+'], stateless: true)]
    public function getFilmCategories(Request $request, int $id): JsonResponse|Response
    {
        $content_type = $request->headers->get('content-type');

        try {
            $film = $this->entityManager->getRepository(Film::class)->find($id);

            if ($film === null) {
                return $this->json(['erreur' => 'Film non trouvé'], 404);
            }

            $categories = $film->getCategories()->toArray();

            if ($content_type == "application/hal+json" || $content_type == "application/hal+xml") {
                $categorie = json_decode($this->serializer->serialize($categories, "json"), true);
                
                $categories = array();

                $categories["links"] = array(
                    "href" => "/" . $id . "/categories",
                    "rel" => "film",
                    "type" => "GET"
                );
                foreach ($categorie as $c) {
                    $categories[] = array(
                        "id" => $c["id"],
                        "libelle" => $c["libelle"],
                    );
                }
                return $this->handleResponse($content_type, (new Film()), $categories);
            }
            $categorie = json_decode($this->serializer->serialize($categories, "json"), true);
            $categories = array();

            foreach ($categorie as $c) {
                $categories[] = array(
                    "id" => $c["id"],
                    "libelle" => $c["libelle"],
                );
            }
            return $this->handleResponse($content_type, (new Film()), $categories);
        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 500);
        }
    }

    #[Route('/categories/{id}/films', name: '_get_category_films', methods: ['GET'], requirements: ['id' => '\d+'], stateless: true)]
    public function getCategoryFilms(Request $request, int $id): JsonResponse|Response
    {
        $content_type = $request->headers->get('content-type');

        try {
            $category = $this->entityManager->getRepository(Categorie::class)->find($id);

            if ($category === null) {
                return $this->json(['erreur' => 'Catégorie non trouvée'], 404);
            }

            $film = $category->getFilm()->toArray();


            if ($content_type == "application/hal+json" || $content_type == "application/hal+xml") {
                $films = json_decode($this->serializer->serialize($film, "json"), true);
                
                $film = array();

                $film["links"] = array(
                    "href" => "/categories/" . $id . "/films",
                    "rel" => "categories",
                    "type" => "GET"
                );
                foreach ($films as $f) {
                    $film[] = array(
                        "id" => $f["id"],
                        "nom" => $f["nom"],
                        "description" => $f["description"],
                        "date" => (new \DateTime($f["date"]))->format("Y-m-d"),
                        "note" => $f["note"],
                    );
                }
                return $this->handleResponse($content_type, (new Film()), $film);
            }
            $films = json_decode($this->serializer->serialize($film, "json"), true);
            $film = array();

            foreach ($films as $f) {
                $film[] = array(
                    "id" => $f["id"],
                    "nom" => $f["nom"],
                    "description" => $f["description"],
                    "date" => (new \DateTime($f["date"]))->format("Y-m-d"),
                    "note" => $f["note"],
                );
            }
            return $this->handleResponse($content_type, (new Film()), $film);
        } catch (\Exception $e) {
            return $this->json(['erreur' => $e->getMessage()], 500);
        }
    }

    // Fonction pour gérer la réponse en fonction du content-type
    private function handleResponse(string $contentType, Film $film, $halfilm = null): JsonResponse|Response
    {
        switch ($contentType) {
            case 'application/hal+json':
                return $this->json($halfilm, 200, ['content-type' => 'application/hal+json']);
            case 'application/json':
                // Convertit l'objet Film en tableau associatif pour la réponse JSON
                if (empty($film))
                    return $this->json(json_decode($this->serializer->serialize($film, "json")), 200, ['content-type' => 'application/json']);
                else
                    return $this->json(json_decode($this->serializer->serialize($halfilm, "json")), 200, ['content-type' => 'application/json']);
            case 'application/hal+xml':
            // dd($halfilm);
                return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($halfilm, "json"), true), 'xml', ['xml_root_node_name' => 'films']), Response::HTTP_OK, ['content-type' => 'application/hal+xml']);
            case 'application/xml':
                // Vous devez implémenter la conversion XML ici si nécessaire
                if (empty($film))
                    return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($film, "json"), true), 'xml', ['xml_root_node_name' => 'films']), Response::HTTP_OK, ['content-type' => 'application/xml']);
                else
                    return new Response((new XmlEncoder())->encode(json_decode($this->serializer->serialize($halfilm, "json"), true), 'xml', ['xml_root_node_name' => 'films']), Response::HTTP_OK, ['content-type' => 'application/xml']);
            default:
                return $this->json(['erreur' => "Le content-type doit être 'application/json' 'application/hal+json' 'application/hal+xml' ou 'application/xml'"], 422);
        }
    }

    
}

