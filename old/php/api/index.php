<?php

$_GET["films"] = preg_replace('/^\//', '', preg_replace('/api\/index.php\//', '', $_SERVER['PHP_SELF']));

require_once(__DIR__ . '/class/class_film.php');
// /film/[0-9]/nom/description/etc
/**
 * [0] =>
 * [1] => films
 * [2] =>
 */
try {
    if (!empty($_GET["films"])) {
        $url = explode('/', $_GET["films"]);
        $contenteType = $_SERVER["CONTENT_TYPE"];
        $method = $_SERVER["REQUEST_METHOD"];
        header('HTTP/1.0 200 ' . $contenteType);
        
        $films = new CFilms($method, $contenteType);
        
        switch ($url[0]) {
            case 'films':
                print_r($films->GetFilms());
                break;
            case 'film':
                # code...
                break;
            default:
                throw new Exception("Validation Impossible", 422);
                break;
        }

    }
    else {
        throw new Exception("Ressource introuvable", 404);
        
    }
} catch (\Throwable $th) {
    print_r(array(
        $th->getMessage()
    ));
}


?>