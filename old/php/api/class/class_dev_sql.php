<?php

class dev_SQL
{
    protected $ID_CNX = 0;
    protected $ID_RES = 0;
    protected $IND_CURS = 0;

    public function __construct() {
        $this->connect();
    }

    public function unhtmlentities ($string): string {
        $trans_tbl = get_html_translation_table (HTML_ENTITIES);
        $trans_tbl = array_flip ($trans_tbl);
        return strtr ($string, $trans_tbl);
    }


    public function connect(): void {
        $StrCnx = "host=database user=postgres password=web_service dbname=web_service";
        var_dump(pg_connect()); die;
        $this->ID_CNX = pg_connect($StrCnx);
        if (false === $this->ID_CNX) {
            echo "La connexion &agrave; la base de donn&eacute;e a echou&eacute;e.";
            exit;
        }
    }
    
    // Liberation de la memoire d'un résultat
    public function free_result (): void {
        pg_free_result ($this->ID_RES);
        $this->ID_RES  = 0;
        $this->IND_CURS = 0;
    } // free_result ()

    // Execution requete
    public function query ($Query): bool {
        if ($this->ID_RES != 0 && $this->ID_RES != true) {
            $this->free_result ();
        }
        $this->IND_CURS = 0;
        // echo " $Query <br>";

        $this->ID_RES = pg_query($this->ID_CNX, $this->unhtmlentities($Query));
        if (! $this->ID_RES) {
          echo " $Query <br>";
            $this->halt ("Invalid SQL:\n".  $this->unhtmlentities($Query));
            return false;
        }

        return true;
    } // Execution requete ()



    public function queryParams($Query, $Params): bool {
        //echo " $Query <br>";
        if ($this->ID_RES != 0 && $this->ID_RES != true) {
            $this->free_result ();
        }
        $this->IND_CURS = 0;

        $this->ID_RES = pg_query_params($this->ID_CNX, $this->unhtmlentities($Query), $Params);
         
        if (!$this->ID_RES) {
            $this->halt ("Invalid SQL:\n".  $this->unhtmlentities($Query) . "\n\n" . print_r($Params, true));
            return false;
        }

        return true;
    } // Execution requete ()

    public function loadData($_query = "", $_params = null): bool|array {

        if($_query === "") {
          return false;
        }
        // exécution de la requete
        $exec = null;
        if ($_params !== null) {
            $exec = $this->queryParams($_query, $_params);
        } else {
            $exec = $this->query($_query);
        }
        if ($exec === false) {
          return $exec;
        }

        return $this->fetch_array_assoc();
    } // Execute la requete et retourne un tuple

    public function loadArray($_query = "", $_params = null): bool|array
    {
        if($_query === "") return false;

        // exécution de la requete
        $exec = null;
        if ($_params !== null) {
            $exec = $this->queryParams($_query, $_params);
        } else {
            $exec = $this->query($_query);
        }
        if ($exec === false) return $exec;

        $res = array();
        while($infos = $this->fetch_array_assoc()) $res[] = $infos;

        return $res;
    } // Execute la requete et retourn un tableau



   







    // Acquisition du prochain enregistrement dans un tableau
    public function fetch_array_assoc(): array|bool {
        $TmpArray = @pg_fetch_array ($this->ID_RES, $this->IND_CURS, PGSQL_ASSOC);
        $this->IND_CURS++;
        return $TmpArray;
    } // fetch_array ()

  
  
    // Fonction halt
    function halt ($Msg): void {
        echo "<pre><b>Database error: </b>$Msg<br>\n<b>Postgresql Error</b><pre>" . pg_last_error($this->ID_CNX) . "<br></pre>\n";
    } // halt ()

  
  
} // dev_SQL
