const dev_SQL  = require('./class_dev_sql')
const { json2xml } = require('xml-js')
const fs = require('fs');

class CFilms {
    #_pt = null
    
    #content_type = null
    #id = null
    #nom = null
    #description = null
    #date = null
    #note = null

    constructor(_id=null, _content_type=null) {
        this.#id = _id
        this.#content_type = _content_type
        this.#_pt = new dev_SQL()
    }

    get Films() {
        let query = "select * from Film"
        let res = this.#_pt.loadArray(query, []) 
        return this.ContentType(res)
    }

    ContentType(arg) {
        if (!/application\/json|application\/xml/.test(this.#content_type)) throw new Error(JSON.stringify({ error: 'Il n\'y a aucun content-type', code: 404 })) 

        switch (arg) {
            case 'application/json': return JSON.stringify(arg)
            case 'application/xml': return json2xml(JSON.stringify(arg))
        }
    }

    /**
     * @param {string} _content_type
     */
    set content_type(_content_type) { this.#content_type = _content_type }

    /**
     * @param {string} _nom
     */
    set nom(_nom) { this.#nom = _nom }

    /**
     * @param {string} _description
     */
    set description(_description) { this.#description = _description }

    /**
     * @param {date} _date
     */
    set date(_date) { this.#date = _date }
    /**
     * @param {int} _note
     */
    set note(_note) { this.#note = _note }
}

module.exports = CFilms

// export default CFilms